import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public  class FizzBuzzTest
{

    @Test
    public void shouldReturnOneWhentheNumberisOne()
    {
        FizzBuzz fizzBuzz = new FizzBuzz();
        String result = fizzBuzz.convert(1);
        assertEquals("1", result);

    }

    @Test
    public void shouldReturnTwoWhentheNumberisTwo()
    {
        FizzBuzz fizzBuzz = new FizzBuzz();
        String result = fizzBuzz.convert(2);
        assertEquals("2", result);

    }

    @Test
    public void shouldReturnFizzWhenTheNumberisMultipleOfThree()
    {
        FizzBuzz fizzBuzz = new FizzBuzz();
        String result = fizzBuzz.convert(9);
        String expected = "Fizz";
        assertEquals(expected, result);
    }

    @Test
    public void shouldReturnBuzzWhenTheNumberisMultipleOfFive()
    {
        FizzBuzz fizzBuzz = new FizzBuzz();
        String result = fizzBuzz.convert(5);
        String expected = "Buzz";
        assertEquals(expected, result);
    }

    @Test
    public void shouldReturnFizzBuzzWhenTheNumberisMultipleofBOTH()
    {
        FizzBuzz fizzBuzz = new FizzBuzz();
        String result = fizzBuzz.convert(30);
        String expected = "FizzBuzz";
        assertEquals(expected, result);
    }

}